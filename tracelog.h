/*
Default values are:
u8 LogTypes = LOG_INFO | LOG_WARNING | LOG_ERROR | LOG_FATAL | LOG_CYCLE;
u8 ExitTypes = LOG_FATAL;
*/
#ifndef TRACELOG_H
#define TRACELOG_H

#if defined(__cplusplus)
extern "C" {
#endif
typedef unsigned char u8;

void SetLogLevel(u8 type);
void SetExitLevel(u8 type);
void SetBitLogLevel(u8 type);
void ClearBitLogLevel(u8 type);
void SetBitExitLevel(u8 type);
void ClearBitExitLevel(u8 type);

void TraceLog(u8 type, const char *text, ...);

// Trace log level
// NOTE: Organized by priority level
typedef enum {
    LOG_TRACE        = 0x01,  // Trace logging, intended for internal use only
    LOG_DEBUG        = 0x02,  // Debug logging, used for internal debugging, it should be disabled on release builds
    LOG_INFO         = 0x04,  // Info logging, used for program execution info
    LOG_WARNING      = 0x08,  // Warning logging, used on recoverable failures
    LOG_ERROR        = 0x10,  // Error logging, used on unrecoverable failures
    LOG_FATAL        = 0x20,  // Fatal logging, used to abort program
    LOG_CYCLE        = 0x40,  // Used in endless loops
} TraceLogLevel;

#if defined(__cplusplus)
}
#endif
#endif // TRACELOG_H
