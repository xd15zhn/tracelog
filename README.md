# reacelog
[raylib](https://gitee.com/xd15zhn/raylib) 中分离出来的C语言日志库。

## 一个简单的例子
```cpp
//main.cpp
#include "tracelog.h"
using namespace std;
int main() {
    int x = 0;
    TRACELOG(LOG_INFO, "Hello world!");
    TRACELOG(LOG_DEBUG, "Hello world!");
    TRACELOG(LOG_WARNING, "Hello world!");
    TRACELOG(LOG_INFO, "ID is 0: %i", x++);
    ClearBitLogLevel(LOG_INFO);
    TRACELOG(LOG_INFO, "ID is 1: %i", x++);
    TRACELOG(LOG_FATAL, "ID is 2: %i", x++);
    TRACELOG(LOG_WARNING, "ID is 3: %i", x++);
    return 0;
}
```
```
# CMakeLists.txt
cmake_minimum_required(VERSION 3.12)
project(untitled)
set(CMAKE_BUILD_TYPE release)
add_executable(${CMAKE_PROJECT_NAME} main.cpp)
list(APPEND CMAKE_PREFIX_PATH "E:/cpplibraries/")
find_package(tracelog REQUIRED)
message(STATUS "tracelog_LIBS: ${tracelog_LIBS}")
message(STATUS "tracelog_VERSION: ${tracelog_VERSION}")
target_link_libraries(${CMAKE_PROJECT_NAME} PUBLIC ${tracelog_LIBS})
target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC ${tracelog_INCLUDE_DIRS})
```
